#!/bin/bash

urls=$(node ./scripts/build_urls.js)

output_file="data.json"
if [ -e "$output_file" ]; then
    rm "$output_file"
else
    touch "$output_file"
fi

echo "$urls" | while IFS= read -r url; do
  json_string=$(node ./scripts/lighthouse.js "$url")
  return_code=$?

  if [ $return_code -eq 0 ]; then
    if [[ -s "$output_file" ]]; then
      echo -n "," >> "$output_file"  # Add a comma if the file is not empty
    else
      echo -n "[" >> "$output_file"  # Add an opening square bracket for the first object
    fi
    echo "$json_string" >> "$output_file"
  else
    echo "Error occurred for URL: $url"
  fi
done

# If the file is not empty, add a closing square bracket to complete the JSON array
if [[ -s "$output_file" ]]; then
  echo "]" >> "$output_file"
fi

# Format the JSON nicely using jq and return a message
jq -S '.' "$output_file" > "${output_file}.tmp" && mv "${output_file}.tmp" "$output_file"
echo "Lighthouse data written to $output_file"
