#! #!/usr/bin/env node

/**
 * @file website_metrics.js
 *
 * Returns Lighthouse audit data for a given page.
 */

import lighthouse from "lighthouse";
/* eslint-disable-next-line import/extensions */
import desktopOpts from "lighthouse/core/config/lr-desktop-config.js";
import * as chromeLauncher from "chrome-launcher";

// Make sure the script was called with an argument.
if (process.argv.length < 3) {
  console.error(
    "Error: Missing argument. Please provide the required argument."
  );
  process.exit(1);
}
const url = process.argv[2];

const getVersionFromURL = () => {
  const match = url.match(/\/(\d+\.\d+)\//);
  return match ? match[1] : "";
};

const onlyCategories = [
  "performance",
  "accessibility",
  "best-practices",
  "seo",
];
const chrome = await chromeLauncher.launch({ chromeFlags: ["--headless"] });
const options = {
  output: "json",
  port: chrome.port,
  onlyCategories,
};

const metrics = { page: url, version: getVersionFromURL(), categories: {} };
const runnerResult = await lighthouse(url, options, desktopOpts);

onlyCategories.forEach((categoryKey) => {
  metrics.categories[categoryKey] =
    runnerResult.lhr.categories[categoryKey].score;
});

console.log(JSON.stringify(metrics));

chrome.kill();
