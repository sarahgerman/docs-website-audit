#!/usr/bin/env node

/*
 * Build a list of URLs for Lighthouse to audit.
 *
 * The URL list includes some of the most popular pages,
 * on all online pages on the site (all supported versions).
 */

const urls = [];

// docs.gitlab.com stable versions
const docsVersions = ["16.3", "16.2", "16.1"];
// versions on the archives site
const archiveVersions = [
  "14.0",
  "14.1",
  "14.2",
  "14.3",
  "14.4",
  "14.5",
  "14.6",
  "14.7",
  "14.8",
  "14.9",
  "14.10",
  "15.0",
  "15.1",
  "15.2",
  "15.3",
  "15.4",
  "15.5",
  "15.6",
  "15.7",
  "15.8",
  "15.9",
  "15.10",
  "15.11",
];
// pages to audit
const pages = ["ee/ci/yaml/index.html", "index.html"];

[...docsVersions, ...archiveVersions].forEach((v) => {
  const baseUrl = docsVersions.includes(v)
    ? "https://docs.gitlab.com"
    : "https://archives.docs.gitlab.com";

  pages.forEach(async (page) => {
    urls.push(`${baseUrl}/${v}/${page}`);
  });
});

console.log(urls.join("\n"));
