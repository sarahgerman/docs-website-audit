# Docs Website Lighthouse Audit Scores

Builds a visualization of the GitLab Docs website's Lighthouse audit
scores, tracked across versions.

This tool uses Chrome's Lighthouse [node.js CLI tool](https://github.com/GoogleChrome/lighthouse#using-the-node-cli)
to run Lighthouse audits against a set of Docs pages, then we have a
Vue-powered frontend application to display this data in a line chart,
showing version-by-version audit scores in four categories: accessibility,
SEO, best practices, and performance.

This is just a proof-of-concept. If we decide this is useful,
we'll want to better automate data collection and move the project
into the GitLab org.

## Run it locally

### Compile Lighthouse data

1. Install dependencies: `npm i`
2. Run Lighthouse against a sampling of pages across all site versions: `make fetch-data`

    This takes a long time if you're sampling a lot of pages. Lighthouse can't be
    [parallelized](https://github.com/GoogleChrome/lighthouse/issues/9845) without
    impacting the performance metrics it calculates.

    Pages we sample are defined in `scripts/build_urls.js`.

This creates a file called data.json in the project root.

### View the data as a chart

1. Start the frontend site: `npm run dev`
1. View the chart at the localhost URL returned from the previous command.

## Known issues

- Some of the data is suspect. Some numbers are pretty different than what you see running Lighthouse in-browser
  (e.g, the straight 100s for best practices). This might be at least partially due to the point below.
- Lighthouse scores can vary due to [a lot of factors beyond our control](https://developers.google.com/web/tools/lighthouse/variability).

## Roadmap

- Run the audit on GitLab CI with [Lighthouse CI](https://github.com/GoogleChrome/lighthouse-ci).
- Consider doing separate charts per page rather than averaging the scores. Currently we're
  averaging scores for the homepage and the CI YAML reference page.
